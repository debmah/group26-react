import React from "react";

const LoginButton = (props) => {
  const { isDisabled, isSubmitting } = props;
  const defaultLabel = props.defaultLabel || "Log In";
  const disabledLabel = props.disabledLabel || "Logging-in...";
  let btn = isSubmitting ? (
    <button disabled className="btn btn-warning">
      {disabledLabel}
    </button>
  ) : (
    <button disabled={isDisabled} type="submit" className="btn btn-primary">
      {defaultLabel}
    </button>
  );
  return <>{btn}</>;
};

export default LoginButton;
