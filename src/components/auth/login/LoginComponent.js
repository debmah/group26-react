import React, { Component } from "react";
import "./login.css";
import LoginButton from "../../common/button/LoginButton";

export default class LoginComponent extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      email: "",
      password: "",
      usernameErr: "",
      passwordErr: "",
      rememberMe: false,
      isSubmitting: false,
      isValidForm: true,
    };
  }

  handleChange = (e) => {
    let { type, name, value, checked } = e.target;
    // console.log("name is >>>>", name);
    // console.log("value is >>>,", value);
    // console.log("checked is >>>,", checked);
    // console.log("type is >>>,", type);
    if (type === "checkbox") {
      value = checked;
    }
    this.setState({
      [name]: value,
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    this.setState({
      isSubmitting: true,
    });

    setTimeout(() => {
      this.setState({
        isSubmitting: false,
      });
    }, 4000);

    console.log("curren state", this.state);
  };
  render() {
    return (
      <div className="loginWrapper">
        <h2>Login</h2>
        <p>Please login to start your session</p>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="username">Username</label>
            <input
              type="text"
              className="form-control"
              name="username"
              id="username"
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              className="form-control"
              name="password"
              id="password"
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group form-check">
            <input
              type="checkbox"
              className="form-check-input"
              name="rememberMe"
              id="rememberMe"
              onChange={this.handleChange}
            />
            <label className="form-check-label" htmlFor="rememberMe">
              Remember Me
            </label>
          </div>
          <LoginButton
            isSubmitting={this.state.isSubmitting}
            isDisabled={!this.state.isValidForm}
            defaultLabel="Log In Mate"
            disabledLabel="Loggin in mate..."
          />
          {/* {btn} */}
        </form>
      </div>
    );
  }
}
