import React, { Component } from "react";
import LoginButton from "../../common/button/LoginButton";
import "./registerComponent.css";

export default class RegisterComponent extends Component {
  constructor() {
    super();
    this.state = {
      isSubmitting: false,
      isValidForm: true,
      username: "",
      fullName: "",
      email: "",
      password: "",
      address: "",
      phoneNumber: "",
    };
  }

  handleChangeRegister = (e) => {
    let { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  };

  handleSubmitRegister = (e) => {
    e.preventDefault();
    console.log("register state data now>>>>", this.state);
    this.setState({
      isSubmitting: true,
    });
    setTimeout(() => {
      this.setState({
        isSubmitting: false,
      });
    }, 4000);
  };
  render() {
    return (
      <div className="loginWrapper">
        <h2>Register</h2>
        <p>Please register to explore the features</p>
        <form onSubmit={this.handleSubmitRegister}>
          <div className="form-group">
            <label htmlFor="fullName">Full Name</label>
            <input
              type="text"
              className="form-control"
              name="fullName"
              id="fullName"
              onChange={this.handleChangeRegister}
            />
          </div>
          <div className="form-group">
            <label htmlFor="email">Email Address*</label>
            <input
              type="text"
              className="form-control"
              name="email"
              id="email"
              onChange={this.handleChangeRegister}
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="username">Username</label>
            <input
              type="text"
              className="form-control"
              name="username"
              id="username"
              onChange={this.handleChangeRegister}
            />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              className="form-control"
              name="password"
              id="password"
              onChange={this.handleChangeRegister}
            />
          </div>
          <div className="form-group">
            <label htmlFor="address">Address</label>
            <input
              type="text"
              className="form-control"
              name="address"
              id="address"
              onChange={this.handleChangeRegister}
            />
          </div>
          <div className="form-group">
            <label htmlFor="phoneNumber">Phone Number</label>
            <input
              type="text"
              className="form-control"
              name="phoneNumber"
              id="phoneNumber"
              onChange={this.handleChangeRegister}
            />
          </div>

          <LoginButton
            isSubmitting={this.state.isSubmitting}
            isDisabled={!this.state.isValidForm}
            defaultLabel="Register"
            disabledLabel="Registering..."
          />
          {/* {btn} */}
        </form>
      </div>
    );
  }
}
