import React from "react";
import "./App.css";
import Header from "./components/common/header/Header";
import LoginComponent from "./components/auth/login/LoginComponent";
import RegisterComponent from "./components/auth/register/RegisterComponent";

function App() {
  return (
    <div className="App">
      <Header />
      <RegisterComponent />
      <LoginComponent />
    </div>
  );
}

export default App;
